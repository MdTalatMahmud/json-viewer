import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
interface City {
  name: string;
  code: string;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  formGroup!: FormGroup;
  cities: City[];
  selectedCity: any;
  selectedCities: any;

  constructor() {
    this.cities = [
      { name: 'New York', code: 'NY' },
      { name: 'Los Angeles', code: 'LA' },
      { name: 'Chicago', code: 'CH' },
      { name: 'Houston', code: 'HO' }
      // Add more cities as needed
    ];

    this.formGroup = new FormGroup({
      selectedCities: new FormControl<City[] | null>([{ name: 'Istanbul', code: 'IST' }])
    });
  }


  title = 'red';
  // cities:any[] = [
  //   { name: 'New York', code: 'NY' },
  //   { name: 'Rome', code: 'RM' },
  //   { name: 'London', code: 'LDN' },
  //   { name: 'Istanbul', code: 'IST' },
  //   { name: 'Paris', code: 'PRS' }
  // ];
  value:number =55

}
